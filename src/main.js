import Vue from 'vue'
import _ from 'lodash'
import {store} from '@/store/index.js'
import App from '@/App'
import MpvueRouterPatch from 'mpvue-router-patch'
//import TopTips from 'mpvue-zanui/src/components/zan/toptips'
//https://github.com/armyja/mpvue-zanui/tree/master/src/pages
//https://github.com/jsjzh/mpvue-zanui
//https://github.com/JJJYY/mpvue-zanui

Vue.config.productionTip = false
Vue.use(MpvueRouterPatch)

const app = new Vue({
  store,
  ...App
})
app.$mount()

export default {
  // 这个字段走 app.json
  config: {
    pages: [
      // 'pages/news/list',
    ], // Will be filled in webpack
    window: {
      backgroundTextStyle: 'light',
      backgroundColor: '#292929',
      backgroundColorTop: '#292929',
      navigationBarBackgroundColor: '#292929',
      navigationBarTitleText: 'moka会员',
      navigationBarTextStyle: 'white',
      navigationBarTextStyle: 'white',
      // enablePullDownRefresh:true,
    },
    tabBar: {
      color: '#999',
      selectedColor: '#d22222',
      backgroundColor: '#fff',
      borderStyle: 'black',
      list: [
      {
        pagePath: 'pages/home/index',
        text: '首页',
        iconPath: 'static/assets/home.png',
        selectedIconPath: 'static/assets/home-selected.png'
      },
      {
        pagePath: 'pages/activity/index',
        text: '活动',
        iconPath: 'static/assets/news.png',
        selectedIconPath: 'static/assets/news-active.png'
      },
      {
        pagePath: 'pages/order/index',
        text: '订单',
        iconPath: 'static/assets/news.png',
        selectedIconPath: 'static/assets/news-active.png'
      },
      {
        pagePath: 'pages/user/index',
        text: '我的',
        iconPath: 'static/assets/quanzi.png',
        selectedIconPath: 'static/assets/quanzi-active.png'
      }]
    }
  }
}
