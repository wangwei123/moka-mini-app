module.exports = [
  {
    path: '/pages/user/index',
    name: 'User',
    config: {
      'navigationBarTitleText': '会员中心'
    }
  },
  {
    path: '/pages/activity/kanjia_success',
    name: 'KanjiaDetail',
    config: {
      'navigationBarTitleText': '查看砍价礼品进度',
      'enablePullDownRefresh': true,
      "usingComponents": {
        "van-steps": "/static/vant/steps/index",
        "van-field": "/static/vant/field/index",
        "van-cell-group": "/static/vant/cell-group/index",
      }
    }
  },
  {
    path: '/pages/activity/kanjia_detail',
    name: 'KanjiaDetail',
    config: {
      'navigationBarTitleText': '砍价详情',
      'enablePullDownRefresh': true,
      "usingComponents": {
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-cell": "/static/vant/cell/index",
        "van-steps":"/static/vant/steps/index",
        "van-cell-group": "/static/vant/cell-group/index",
      }
    }
  },
  {
    path: '/pages/activity/kanjia_record',
    name: 'KanjiaRecord',
    config: {
      'navigationBarTitleText': '砍价记录',
      "usingComponents": {
        "van-icon": "/static/vant/icon/index",
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-button": "/static/vant/button/index",
        "van-tab": "/static/vant/tab/index",
        "van-tabs": "/static/vant/tabs/index",
      }
    }
  },{
    path: '/pages/user/gift_card/gift_card_detail',
    name: 'GiftCardDetail',
    config: {
      'navigationBarTitleText': '订单详情',
      "usingComponents": {
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-icon": "/static/vant/icon/index",
      }
    }
  },{
    path: '/pages/user/gift_card/gift_card_get',
    name: 'GiftCardGet',
    config: {
      'navigationBarTitleText': '领取礼品卡',
      "usingComponents": {
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-icon": "/static/vant/icon/index",
        "van-dialog": "/static/vant/dialog/index",
        "van-field": "/static/vant/field/index"
      }
    }
  },{
    path: '/pages/user/gift_card/gift_card_list',
    name: 'GiftCardList',
    config: {
      'navigationBarTitleText': '礼品卡列表',
      "usingComponents": {
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-icon": "/static/vant/icon/index",
      }
    }
  },{
    path: '/pages/user/gift_card/gift_card',
    name: 'GiftCard',
    config: {
      'navigationBarTitleText': '礼品卡',
      "usingComponents": {
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
      }
    }
  },{
    path: '/pages/user/gift_card/purchase_record',
    name: 'GiftCard',
    config: {
      'navigationBarTitleText': '购买记录',
      "usingComponents": {
        
      }
    }
  },{
    path: '/pages/user/gift_card/receive_record',
    name: 'GiftCard',
    config: {
      'navigationBarTitleText': '领取记录',
      "usingComponents": {
        
      }
    }
  },{
    path: '/pages/user/bind_member_card',
    name: 'BindMemberCard',
    config: {
      'navigationBarTitleText': '绑卡',
    }
  }, {
    path: '/pages/user/get_user_info',
    name: 'GetUserInfo',
    config: {
      'navigationBarTitleText': '申请授权',
      "usingComponents": {
        "van-icon": "/static/vant/icon/index",
      }
    }
  }, {
    path: '/pages/user/invitation',
    name: 'Invitation',
    config: {
      'navigationBarTitleText': '邀请有奖'
    }
  }, {
    path: '/pages/user/new_user_gifts',
    name: 'RegisterUser',
    config: {
      'navigationBarTitleText': '新人礼包'
    }
  },{
    path: '/pages/user/coupons',
    name: 'Coupons',
    config: {
      'navigationBarTitleText': '优惠券'
    }
  }, {
    path: '/pages/user/coupon_center',
    name: 'CouponCenter',
    config: {
      'navigationBarTitleText': '领券中心'
    }
  }, {
    path: '/pages/user/pay_code',
    name: 'Cardcode',
    config: {
      'navigationBarTitleText': '付款码'
    }
  }, {
    path: '/pages/user/description',
    name: 'Description',
    config: {
      'navigationBarTitleText': '会员说明'
    }
  }, {
    path: '/pages/user/feedback',
    name: 'Feedback',
    config: {
      'navigationBarTitleText': '意见反馈'
    }
  }, {
    path: '/pages/user/member_info',
    name: 'MemberInfo',
    config: {
      'navigationBarTitleText': '会员信息'
    }
  }, {
    path: '/pages/user/points_mall',
    name: 'PointsMall',
    config: {
      'navigationBarTitleText': '积分商城',
      "usingComponents": {
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-icon": "/static/vant/icon/index",
      }
    }
  }, {
    path: '/pages/user/points_product_detail',
    name: 'PointsProductDetail',
    config: {
      'navigationBarTitleText': '积分商品详情'
    }
  }, {
    path: '/pages/user/points_use_record',
    name: 'pointsUseRecord',
    config: {
      'navigationBarTitleText': '积分兑换记录'
    }
  }, {
    path: '/pages/user/modify_password',
    name: 'ModifyPassword',
    config: {
      'navigationBarTitleText': '修改密码'
    }
  }, {
    path: '/pages/user/transaction_record',
    name: 'TransactionRecord',
    config: {
      'onReachBottomDistance': 50,
      'navigationBarTitleText': '交易记录'
    }
  }, {
    path: '/pages/user/wechat_recharge',
    name: 'WechatRecharge',
    config: {
      'navigationBarTitleText': '微信充值'
    }
  }, {
    path: '/pages/activity/index',
    name: 'Activity',
    config: {
      'navigationBarTitleText': '活动'
    }
  }, {
    path: '/pages/activity/miaosha_balance',
    name: 'MiaoshaBalance',
    config: {
      'navigationBarTitleText': '结算'
    }
  }, {
    path: '/pages/activity/group/launch_group',
    name: 'LaunchGroup',
    config: {
      'navigationBarTitleText': '发起拼团',
      "usingComponents": {
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-icon": "/static/vant/icon/index",
      }
    }
  }, {
    path: '/pages/activity/group/group_pay',
    name: 'GroupPay',
    config: {
      'navigationBarTitleText': '拼团支付',
      "usingComponents": {
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-icon": "/static/vant/icon/index",
      }
    }
  }, {
    path: '/pages/activity/group/group_detail',
    name: 'GroupPay',
    config: {
      'navigationBarTitleText': '拼团详情',
      "usingComponents": {
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-icon": "/static/vant/icon/index",
        "van-cell": "/static/vant/cell/index",
        "van-cell-group": "/static/vant/cell-group/index"
      }
    }
  }, {
    path: '/pages/activity/group/my_group',
    name: 'Group',
    config: {
      'navigationBarTitleText': '我的拼团',
      "usingComponents": {
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-icon": "/static/vant/icon/index",
        "van-tab": "/static/vant/tab/index",
        "van-tabs": "/static/vant/tabs/index",
        "van-progress": "/static/vant/progress/index",
      }
    }
  }, {
    path: '/pages/activity/group/join_group_pay',
    name: 'JoinGroupPay',
    config: {
      'navigationBarTitleText': '拼团支付',
      "usingComponents": {
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-icon": "/static/vant/icon/index",
      }
    }
  },{
    path: '/pages/order/index',
    name: 'Order',
    config: {
      'navigationBarTitleText': '订单',
      'onReachBottomDistance': 50,
      "usingComponents": {
        "van-button": "/static/vant/button/index",
      }
    }
  }, {
    path: '/pages/home/index',
    name: 'Home',
    config: {
      'navigationBarTitleText': '点单'
    }
  }, {
    path: '/pages/home/shop',
    name: 'Shop',
    config: {
      'navigationBarTitleText': '门店详情'
    }
  }, {
    path: '/pages/home/goods_list',
    name: 'GoodsList',
    config: {
      'navigationBarTitleText': '商品列表'
    }
  }, {
    path: '/pages/home/balance',
    name: 'Balance',
    config: {
      'navigationBarTitleText': '提交订单'
    }
  }, {
    path: '/pages/home/order_detail',
    name: 'OrderDetail',
    config: {
      'navigationBarTitleText': '订单详情',
      "usingComponents": {
        "van-button": "/static/vant/button/index",
      }
    }
  }, {
    path: '/pages/home/order_coupon',
    name: 'OrderCoupon',
    config: {

      'navigationBarTitleText': '使用优惠券',
    }
  }, {
    path: '/pages/user/wechat_card_activate',
    name: 'WechatCardActivate',
    config: {
      'navigationBarTitleText': '会员卡激活'
    }
  }, {
    path: '/pages/user/micro_pay',
    name: 'MicroPay',
    config: {
      'navigationBarTitleText': '收银台',
    }
  }, {
    path: '/pages/user/micro_pay_coupon',
    name: 'MicroPayCoupon',
    config: {
      'navigationBarTitleText': '选择优惠券',
      "usingComponents": {
        "van-icon": "/static/vant/icon/index",
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
      }
    }
  }, {
    path: '/pages/user/check_in',
    name: 'CheckIn',
    config: {
      'navigationBarTitleText': '签到',
      "usingComponents": {
        "van-button": "/static/vant/button/index",
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-icon": "/static/vant/icon/index",
      }
    }
  }, {
    path: '/pages/user/sale_cards',
    name: 'SaleCards',
    config: {
      'navigationBarTitleText': '在线购卡',
      "usingComponents": {
        "van-icon": "/static/vant/icon/index",
      }
    }
  },
  {
    path: '/pages/user/share_coupon',
    name: 'ShareCoupon',
    config: {
      'navigationBarTitleText': '领取分享券',
      "usingComponents": {
        "van-icon": "/static/vant/icon/index",
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-button": "/static/vant/button/index",
      }
    }
  },
  {
    path: '/pages/activity/kanjia',
    name: 'Kanjia',
    config: {
      'navigationBarTitleText': '砍价',
      "usingComponents": {
        "van-icon": "/static/vant/icon/index",
        "van-row": "/static/vant/row/index",
        "van-col": "/static/vant/col/index",
        "van-field": "/static/vant/field/index",
        "van-popup":"/static/vant/popup/index",
        "van-button": "/static/vant/button/index",
        "van-cell-group": "/static/vant/cell-group/index",
        "van-action-sheet":"/static/vant/action-sheet/index",
      }
    }
  }
]
