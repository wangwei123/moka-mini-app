import Vue from 'vue'
import Vuex from 'vuex'

import user from './module/user'
import shop from './module/shop'
import goods from './module/goods'
import fight_group from './module/fight_group'
import miao_sha from './module/miao_sha'
import points_mall from './module/points_mall'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules:{
    user,
    shop,
    goods,
    fight_group,
    miao_sha,
    points_mall
  }
})
