import _ from 'lodash'
//import moment from 'moment'
import api from '@/utils/api'

const state = {
  fight_group_tpls: [],
  current_fight_group_tpl: {},
  fighting_group: {},
  my_order: {},
  end_time_second: 0,
  total_fighting_num: 0,
  fighting_groups: [],
  already_group:false,
}

const getters = {
  fight_group_tpls: state => state.fight_group_tpls,
  current_fight_group_tpl: state => state.current_fight_group_tpl,
  fighting_group: state => state.fighting_group,
  my_order: state => state.my_order,
  end_time_second: state => state.end_time_second,
  total_fighting_num: state => state.total_fighting_num,
  fighting_groups: state => state.fighting_groups,
  already_group: state => state.already_group,
}

const mutations = {
  SET_FIGHT_GROUP_TPLS(state, fight_group_tpls) {
    state.fight_group_tpls = fight_group_tpls
  },
  SET_CURRENT_FIGHT_GROUP_TPL(state, current_fight_group_tpl) {
    state.current_fight_group_tpl = current_fight_group_tpl
  },
  SET_FIGHTING_GROUP(state, fighting_group) {
    state.fighting_group = fighting_group
  },
  SET_MY_ORDER(state, order) {
    state.my_order = order
  },
  SET_END_TIME_SECOND(state, end_time_second) {
    state.end_time_second = end_time_second
  },
  SET_TOTAL_FIGHTING_NUM(state, total_fighting_num) {
    state.total_fighting_num = total_fighting_num
  },
  SET_FIGHTING_GROUPS(state, fighting_groups) {
    state.fighting_groups = fighting_groups
  },
  SET_ALREADY_GROUP(state, already_group) {
    state.already_group = already_group
  }
}

const actions = {
  fetchFightGroupTpls({commit}, params) {
    return new Promise((resolve, reject) => {
      api.findFightGroupTpls(params).then(result=>{
        if (result.code === 0) {
          commit("SET_FIGHT_GROUP_TPLS", result.data)
        }
        resolve(result)
      })
    })
  },
  getFightGroupTpl({commit}, params) {
    return new Promise((resolve, reject) => {
      api.getFightCroupTpl(params).then(result=>{
        if (result.code === 0) {
          commit("SET_CURRENT_FIGHT_GROUP_TPL", result.data)
        }
        resolve(result)
      })
    })
  },
  getFightGroup({commit,rootState}, params) {
    commit("SET_END_TIME_SECOND", 0)
    let user = rootState.user.user
    return new Promise((resolve, reject) => {
      api.getFightCroup(params).then(result=>{
        if (result.code === 0) {
          commit("SET_FIGHTING_GROUP", result.data)

          let end_time_second = (new Date(result.data.end_time)).valueOf() - (new Date()).valueOf()
          commit("SET_END_TIME_SECOND", end_time_second)

          //判断目前用户是否已经拼过此团
          result.data.orders.forEach(order=>{
            if (order.user_id == user.id) {
              commit("SET_ALREADY_GROUP",true)
            }
          })
        }
        resolve(result)
      })
    })
  },
  createFightGroup({commit}, fightGroup) {
    api.createFightGroup(fightGroup).then(result=>{
      if (result.code === 0) {
        wx.navigateTo({url:"fighting_group?fight_group_id="+result.data.id})
      }
    })
  },
  joinFightGroup({commit}, fightGroup) {
    api.joinFightGroup(fightGroup).then(result=>{
      if (result.code === 0) {
        wx.navigateTo({url:"fighting_group?fight_group_id="+result.data.id})
      }
    })
  },
  getTotalFightNum({commit, state}, params) {
    return new Promise((resolve, reject) => {
      api.getTotalFightingNum(params).then(result=>{
        if (result.code === 0) {
          commit("SET_TOTAL_FIGHTING_NUM", result.data)
        }
        resolve(result)
      })
    })
  },
  findFightingGroups({commit, state}, params) {
    return new Promise((resolve, reject) => {
      api.findFightingGroups(params).then(result => {
        console.log('result: ', result)
        if (result.code === 0) {
          result.data.forEach(item => {
            item.need_people_num = item.people_num - item.fighting_num
            console.log('item.need_people_num: ', item.need_people_num)
          })
          commit("SET_FIGHTING_GROUPS", result.data)
          console.log('result.data: ', result.data)
        }
        resolve(result)
      })
    })
  },
  getMyOrderInfo({commit, state}, user_id) {
    return new Promise((resolve, reject) => {
      if (state.fighting_group && state.fighting_group.orders) {
        let order = _.find(state.fighting_group.orders, {user_id, user_id})
        console.log('order: ', order)
        commit("SET_MY_ORDER", order)
        resolve(order)
      }
    })
  }
}

export default{
  state,
  getters,
  mutations,
  actions
}
