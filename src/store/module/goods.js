import api from '@/utils/api'

const state = {
  goods_list:[],
  ranges:[],
  product_attrs:[],
  cart_list:[],
  top_fix_name:'',
  to_view:'',
  active_index: 0,
  current_product:{},
  seasoning_status:false,
  cup_number:0,
  sum_monney:0,
  cart_list_status:false,
  cart_length:'',
  select_coupon:{title:''}
}

const getters = {
  goods_list: state => state.goods_list,
  top_fix_name: state => state.top_fix_name,
  to_view: state => state.to_view,
  ranges: state => state.ranges,
  cart_list: state => state.cart_list,
  product_attrs: state => state.product_attrs,
  active_index: state => state.active_index,
  current_product: state => state.current_product,
  seasoning_status: state => state.seasoning_status,
  cup_number: state => state.cup_number,
  sum_monney: state => state.sum_monney,
  cart_list_status: state => state.cart_list_status,
  cart_length: state => state.cart_length,
  select_coupon: state => state.select_coupon,
}

const mutations = {
  goodsList(state,data){
    state.goods_list = data
    state.top_fix_name = data[0].name
  },
  TOP_FIX_NAME(state,data){
    state.top_fix_name = data
  },
  TO_VIEW(state,data){
    state.to_view = data
  },
  ACTIVE_INDEX(state,data){
    state.active_index = data
  },
  CURRENT_PRODUCT(state,data){
    state.current_product = data
  },
  SEASONING_STATUS(state,data){
    state.seasoning_status = data
  },
  productAttrs(state,data){
    state.product_attrs = data
  },
  CRAT_LIST(state,data){
    state.cart_list = data
  },
  CUP_NUMBER(state,data){
    state.cup_number = data
  },
  SUM_MONNEY(state,data){
    state.sum_monney = data
  },
  ranges(state,data){
    state.ranges = data
  },
  productAttrChecked(state,data){
    state.product_attrs = data
  },
  CART_LIST_STATUS(state,data){
    state.cart_list_status = data
  },
  CART_LENGTH(state,data){
    state.cart_length = data
  },
  selectCoupon(state,data){
    state.select_coupon = data
  },
}

const actions = {
  clearAllData({commit},data){
    commit("CRAT_LIST",[])//清空购物车
    commit("CUP_NUMBER",0)//清空商品数量
    commit("SUM_MONNEY",0)//清空底部的总价格
    commit("CART_LIST_STATUS",false)
  },
  setProductAttrChecked({commit},data){
    commit("productAttrChecked",data)
  },
  setTopFixName({commit},data){
    commit("TOP_FIX_NAME",data)
  },
  setToView({commit},data){
    commit("TO_VIEW",data)
  },
  setActiveIndex({commit},data){
    commit("ACTIVE_INDEX",data)
  },
  setCurrentProduct({commit},data){
    commit("CURRENT_PRODUCT",data)
  },
  setSeasoningStatus({commit},data){
    commit("SEASONING_STATUS",data)
  },
  setCupNumber({commit},data){
    commit("CUP_NUMBER",data)
  },
  setSumMonney({commit},data){
    commit("SUM_MONNEY",data)
  },
  setCartList({commit},data){
    commit("CRAT_LIST",data)
  },
  setCartListStatus({commit},data){
    commit("CART_LIST_STATUS",data)
  },
  setCartLength({commit},data){
    commit("CART_LENGTH",data)
  },
  clearProductAttrs({commit},data){
    data.forEach(item=>{
      item.product_attrs.forEach((product_attr,index)=>{
        if (index == 0) {
          product_attr.checked = true
        } else {
          product_attr.checked = false
        }
      })
    })
    commit("productAttrs",data)
  },
  async setProductAttrs({commit,rootState}){
    let login_info = rootState.user.login_info
    let result = await api.findProductAttrs({brand_id:login_info.brand_id})
    if (result.code == 0) {
      result.data.forEach(item=>{
        item.product_attrs.forEach((product_attr,index)=>{
          if (index == 0) {
            product_attr.checked = true
          } else {
            product_attr.checked = false
          }
        })
      })
      commit("productAttrs",result.data)
    }
  },
  async setGoodsList({commit,rootState}){
    let login_info = rootState.user.login_info
    let result = await api.findProducts({brand_id:login_info.brand_id})
    if (result.code == 0) {
      commit("goodsList",result.data)

      //计算商品滚动的距离
      let ranges = []
      let start = 0
      let end = 0
      let lastEnd = 0
      result.data.forEach((item,index)=>{
        if (index == 0) {
          start = 0
          end = 18+item.products.length * 85 + start
          lastEnd += end
        }else {
          start = lastEnd
          end = 18+item.products.length * 85 + start
          lastEnd = end
        }
        let range = {
          index:index,
          start:(start*0.856).toFixed(2),
          end:(end*0.856).toFixed(2)
        }
        ranges.push(range)
      })
      commit("ranges",ranges)
    }
  },
  setSelectCoupon({commit},data){
    commit("selectCoupon",data)
  },
}

export default{
  state,
  getters,
  mutations,
  actions
}
