import api from '@/utils/api'

const state = {
  miao_sha_goods:{},
}

const getters = {
  miao_sha_goods: state => state.miao_sha_goods,
}

const mutations = {
  MIAO_SHA_GOODS(state,data){
    state.miao_sha_goods = data
  },
}

const actions = {
  setMiaoShaGoods({commit},data){
    commit("MIAO_SHA_GOODS",data)
  },
}

export default{
  state,
  getters,
  mutations,
  actions
}