import api from '@/utils/api'

const state = {
  points_mall_product:{},
}

const getters = {
  points_mall_product: state => state.points_mall_product,
}

const mutations = {
  POINTS_MALL_PRODUCT(state,data){
    state.points_mall_product = data
  }
}

const actions = {
  setPointsMallProduct({commit},data){
    commit("POINTS_MALL_PRODUCT",data)
  }
}

export default{
  state,
  getters,
  mutations,
  actions
}