import api from '@/utils/api'

const state = {
  shops:[],
  shop:{},
  time_ranges:[],
  order_time:'',
  order_type:''
}

const getters = {
  shops: state => state.shops,
  shop: state => state.shop,
  time_ranges: state => state.time_ranges,
  order_time: state => state.order_time,
  order_type: state => state.order_type,
}

const mutations = {
  SHOPS(state,data){
    state.shops = data
  },
  SHOP(state,data){
    state.shop = data
  },
  TIME_RANGES(state,data){
    state.time_ranges = data
  },
  ORDER_TIME(state,data){
    state.order_time = data
  },
  ORDER_TYPE(state,data){
    state.order_type = data
  }
}

const actions = {
  setOrderType({commit},data){
    commit("ORDER_TYPE",data)
  },
  setTimeRanges({commit},data){
    commit("TIME_RANGES",data)
  },
  setOrderTime({commit},data){
    commit("ORDER_TIME",data)
  },
  setShop({commit},data){
    commit("SHOP",data)
  },
  setShops({commit},data){
    commit("SHOPS",data)
  },
}

export default{
  state,
  getters,
  mutations,
  actions
}
