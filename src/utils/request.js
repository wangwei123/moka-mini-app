import wx from 'wx'
import Fly from 'flyio'

const request = new Fly()

const basePath = 'https://api.moka360.vip/app_api'
//const basePath = 'http://localhost:4000/app_api'

request.config.baseURL = basePath
request.interceptors.request.use((request) => {
  wx.showNavigationBarLoading()
  //config.headers["X-Tag"]="flyio";
  return request
})

request.interceptors.response.use(
  (response, promise) => {
    wx.hideNavigationBarLoading()
    if (response.data.code !== 0) {
      if (response.data.msg !== '会员用户不存在') {
        wx.showToast({title: response.data.msg, icon: 'none'})
      }
      return promise.resolve(response.data)
    }

    return promise.resolve(response.data)
  },
  (err, promise) => {
    wx.hideNavigationBarLoading()
    wx.showToast({
      title: err.message,
      icon: 'none'
    })
    return promise.resolve(response.data)
  }
)

export default request
