//展示错误信息
export const showErrorMsg = (msg) => {
  wx.showToast({
    title: msg,
    icon: 'none'
  })
}

export const showSucToask = (msg) => {
  wx.showToast({
    title: msg,
    icon: 'success'
  })
}
